--- svelte
gg [svelte get started](https://www.google.com/search?q=svelte+get+started)
ref svelte.dev aka Svelte API Doc https://svelte.dev/docs/introduction
ref codingthesmartway https://www.codingthesmartway.com/the-svelte-3-quickstart-tutorial/


== svelte.dev aka Svelte API Doc https://svelte.dev/docs/introduction
> SvelteKit calls `Svelte compiler` to convert your `.svelte` files
> into `.js` files that create the DOM and `.css` files that style it

> SvelteKit uses `Vite` to build your code

TODO is `Vite` the `Svelte compiler`?

> SvelteKit supports Server-side rendering SSR by default

> ask for help in [Discord chatroom](https://svelte.dev/chat)
> and on [Stack Overflow](https://stackoverflow.com/questions/tagged/svelte)


== interactive tutorial https://learn.svelte.dev

= command log @ init svelte app @ skeleton app
npm init svelte 
npm init svelte learnsveltedev  # select > Skeleton project
```consoleoutput
Next steps:
1: cd learnsveltedev
2: npm install (or pnpm install, etc)
3: git init && git add -A && git commit -m "Initial commit" (optional)
4: npm run dev -- --open

To close the dev server, hit Ctrl-C
```

= command log @ run it
cd learnsveltedev
    npm i ; npm run dev 
    npm i ; npm run dev -- --open  # auto open webbrowser

= command log @ init svelte app @ wordguessing app
npm init svelte learnsveltedev--word-guessing-demo  # select > SvelteKit demo app

= follow the interactive guide at https://learn.svelte.dev/tutorial/your-first-component
code will place under ...thisrepo/src/learnsveltedev--tutorial

== TODO Svelte examples https://svelte.dev/docs/introduction/examples
== TODO Svelte online REPL https://svelte.dev/docs/introduction//repl
== TODO fully-featured environment @ Svelte on StackBlitz https://sveltekit.new



--- TODO svelte w/ vercel
gg svelte get started vercel
ref https://thenewstack.io/vercel-and-svelte-a-perfect-match-for-web-developers/
